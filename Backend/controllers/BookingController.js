const Passenger = require('../models/passenger')
const Bus = require('../models/Bus')
const Passindex = (req,res,next) => {
    Passenger.find()
    .then(response => {
        res.json({
            response
        })
    })
    .catch(error => {
        res.json({message:'An Error Occured'
        })
    })
}


const Passshow = (req,res,next) => {
    let PassID=req.body.PassID
    Passenger.findById(PassID)
    .then(response => {
        res.json({
            response
        })
    })
    .catch(error => {
        res.json({
            message:'An Error Occured'
        })
    })
}

 
const Passstore = (req,res,next) => {
    let BusID=req.body.BusID.toString();
    let PassSeats=req.body.PassSeat
    let passenger = new Passenger({
        UserID:req.body.UserID,
        PassName: req.body.PassName,
        PassAge: req.body.PassAge,
        PassGender: req.body.PassGender,
        PassSeat: PassSeats,
        BusID:BusID
    })

    Bus.findById(BusID)
    .then(response => {
        passenger.save();
        let updatedData = {
            BusSeats:Number(response.BusSeats-1),
            'IsBooked.$.status':"closed"
        }
        Bus.updateOne({_id:BusID,'IsBooked.name':parseInt(PassSeats)}, {$set : updatedData})
        .then((response) => {
            res.json({
                message:'Bus Details Updated!'
            })
        })
        .catch(error =>{
            res.json({
                message:'Bus Detail Updation Error'
            })
        }) 
    })
    .catch(error => {
        res.json({
            message:"Error finding the bus"
        })
    })

    
}

const Passupdate=(req,res,next)=>{
    let PassID=req.body.PassID

    let updatedData = {
        PassName:req.body.PassName,
        PassAge:req.body.PassAge 
    }

    Passenger.findByIdAndUpdate(PassID, {$set :updatedData}, {new:true}) 
    .then(() => {
        res.json({
            message:'Passenger Details Updated!'
        })
    })
    .catch(error =>{
        res.json({
            message:'Passenger detail Updation Error'
        })
    })
}


const Passdestroy = (req,res,next) => {
    let PassID=req.params.PassID
    Passenger.findById(PassID)
    .then(response => {
        var passSeats=response.PassSeat;
        var BusID=response.BusID;
        
        Bus.findById(BusID)
        .then(response => {
            let updatedData={
                BusSeats:response.BusSeats+1,
                "IsBooked.$.status":"Open"
            } 
            Bus.updateOne({_id:BusID,"IsBooked.name":parseInt( passSeats)},{$set : updatedData})
            .then(() => {
                Passenger.findByIdAndRemove(PassID)
                .then(() => {
                    res.json({
                        message:'Passenger Deleted Succesfully'
                    })
                })
                .catch(error =>{
                    res.json({
                        message:'Passenger Deletion Error Occured'
                    })   
                })
            }) 
        })
        .catch(error =>{
            res.json({
                message:'Error Finding Bus!'
            })   
        })
    })
}
const PassClear = (req,res,next) => {
    Passenger.deleteMany({})
    .then(() => {
        res.json({
            message:"all passengers cleared!"
        })
    })
}

module.exports = {
    Passindex, Passshow, Passstore, Passupdate, Passdestroy,PassClear
}

