const User = require('../models/User')
const Passenger = require('../models/passenger')
const Login=(req,res,next) => {
    let Name=req.body.Name
    let Password=req.body.Password
    User.findOne({Name:Name,Password:Password})
    .then(response => {
        if(response!=null) {
            res.json({
                "res":true,
                response
            })
        }
        else {
            res.json({
                "response":false
            }) 
        }
    })
    .catch(error => {
        res.json({
            message:'An Error Occured'
        })
    })
}

const SignUp = (req,res,next) => {
    let New = new User({
        Email:req.body.Email,
        Name:req.body.Name,
        Password:req.body.Password,
        Phone:req.body.Phone,
        Address:req.body.Address,
        City:req.body.City,
        Pincode:req.body.Pincode
    })
    New.save()
    .then(response => {
        res.json({
            message:'New User Details Added!'
        })
    })

}

const ShowTickets = (req,res,next) => {
    let UserID=req.params.UserID
    Passenger.find({UserID:UserID})
    .then(response => {
        res.json({
            response
        })
    })
    .catch(error => {
        res.json({
            message:'Show Tickets Error Occured'
        })
    })
}

const Update = (req,res,next) => {
    let UserID=req.body.UserID
    let UpdatedData = {
        Email:req.body.Email,
        Name:req.body.Name,
        Password:req.body.Password,
        Phone:req.body.Phone,
        Address:req.body.Address,
        City:req.body.City,
        Pincode:req.body.Pincode
    }
    User.findByIdAndUpdate(UserID,{$set : UpdatedData}, {new:true})
    .then(() => {
        res.json({
            message:'User Details Updated!'
        })
    })
    .catch(error =>{
        res.json({
            message:'User detail Updation Error'
        })
    })
}

const ShowUser = (req,res,next) => {
    let UserID=req.params.UserID
    User.findById(UserID)
    .then(response => {
        res.json({
            response
        })
    })
    .catch(error => {
        res.json({
            message:'Show User Error Occured'
        })
    })
}

const Index = (req,res,next) => {
    User.find()
    .then(response => {
        res.json({
            response
        })
    })
    .catch(error => {
        res.json({message:'An Error Occured'
        })
    })
}

const Destroy = (req,res,next) => {
    let ID=req.params.UserID
    User.findByIdAndRemove(ID)
    .then(() => {
        res.json({
            message:'User Deleted Succesfully'
        })
    })
    .catch(error =>{
        res.json({
            message:'User Deletion Error Occured'
        })   
    })
}

module.exports={
    Login,SignUp,Index,Destroy,Update,ShowTickets,ShowUser
}