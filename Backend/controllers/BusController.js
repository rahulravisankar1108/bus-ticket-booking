const Bus = require('../models/Bus')
const Passenger = require('../models/passenger')
require('dotenv/config')

const index = (req,res,next) => {
    Bus.find()
    .then(response => {
        res.json({
            response
        })
    })
    .catch(error => {
        res.json({
            message:'An Error Occured'
        })
    })
}


const show = (req,res,next) => {
    let BusID=req.params.BusID
    console.log(BusID)
    Bus.findById(BusID)
    .then(response => {
        res.json({
            response
        })
    })
    .catch(error => {
        res.json({
            message:'An Error Occured'
        })
    })
}

 
const store = (req,res,next) => {
    let arr=[
        {
            "name":1,
            "status":"Open"
        },
        {
            "name":2,
            "status":"Open"
        },
        {
            "name":3,
            "status":"Open"
        },
        {
            "name":4,
            "status":"Open"
        },
        {
            "name":5,
            "status":"Open"
        },
        {
            "name":6,
            "status":"Open"
        },
        {
            "name":7,
            "status":"Open"
        },
        {
            "name":8,
            "status":"Open"
        },
        {
            "name":9,
            "status":"Open"
        },
        {
            "name":10,
            "status":"Open"
        },
        {
            "name":11,
            "status":"Open"
        },
        {
            "name":12,
            "status":"Open"
        },
        {
            "name":13,
            "status":"Open"
        },
        {
            "name":14,
            "status":"Open"
        },
        {
            "name":15,
            "status":"Open"
        },
        {
            "name":16,
            "status":"Open"
        },
        {
            "name":17,
            "status":"Open"
        },
        {
            "name":18,
            "status":"Open"
        },
        {
            "name":19,
            "status":"Open"
        },
        {
            "name":20,
            "status":"Open"
        },
        {
            "name":21,
            "status":"Open"
        },
        {
            "name":22,
            "status":"Open"
        },
        {
            "name":23,
            "status":"Open"
        },
        {
            "name":24,
            "status":"Open"
        },
        {
            "name":25,
            "status":"Open"
        },
        {
            "name":26,
            "status":"Open"
        },
        {
            "name":27,
            "status":"Open"
        },
        {
            "name":28,
            "status":"Open"
        },
        {
            "name":29,
            "status":"Open"
        },
        {
            "name":30,
            "status":"Open"
        },
        {
            "name":31,
            "status":"Open"
        },
        {
            "name":32,
            "status":"Open"
        },
        {
            "name":33,
            "status":"Open"
        },
        {
            "name":34,
            "status":"Open"
        },
        {
            "name":35,
            "status":"Open"
        },
        {
            "name":36,
            "status":"Open"
        },
        {
            "name":37,
            "status":"Open"
        },
        {
            "name":38,
            "status":"Open"
        },
        {
            "name":39,
            "status":"Open"
        },
        {
            "name":40,
            "status":"Open"
        }

    ];
    let bus = new Bus({
        BusName: req.body.BusName,
        BusType: req.body.BusType,
        BusStartTiming: req.body.BusStartTiming,
        BusEndTiming: req.body.BusEndTiming,
        BusDate: req.body.BusDate,
        BusSource: req.body.BusSource,
        BusDestination: req.body.BusDestination,
        BusFeatures: req.body.BusFeatures,
        BusSeats: req.body.BusSeats,
        BusFare: req.body.BusFare,
        IsBooked : arr
        // BusRating:req.body.BusRating
    })
    bus.save()
    .then(response => {
        res.json({
            message:'Bus added Succesfully!'
        })
    })
    .catch(error => {
        res.json({
            message:'Bus Couldnt save'
        })
    })
}


const update=(req,res,next)=>{
    let BusID=req.body.BusID

    let updatedData = {
        BusName:req.body.BusName,
        BusType: req.body.BusType,
        BusStartTiming: req.body.BusStartTiming,
        BusEndTiming: req.body.BusEndTiming,
        BusDate: req.body.BusDate,
        BusSource: req.body.BusSource,
        BusDestination: req.body.BusDestination,
        BusFeatures: req.body.BusFeatures,
        BusSeats: req.body.BusSeats,
        BusFare: req.body.BusFare 
    }

    Bus.findByIdAndUpdate(BusID, {$set :updatedData}, {new:true}) 
    .then(() => {
        res.json({
            message:'Bus Details Updated!'
        })
    })
    .catch(error =>{
        res.json({
            message:'Bus detail Updation Error'
        })
    })
}


const destroy = (req,res,next) => {
    let BusID=req.params.BusID
    Bus.findByIdAndRemove(BusID)
    .then(() => {
        Passenger.find(BusID)
        .then(response => {
            
        })
        res.json({
            message:'Bus Deleted Succesfully'
        })
    })
    .catch(error =>{
        res.json({
            message:'Bus Deletion Error Occured'
        })   
    })
}

const countTickets = (req,res,next) => {
    let busID=req.params.BusID
    Bus.findById(busID)
    .then(response => {
        let arr=response.IsBooked
        var j=0
        var openTickets = []
        for(var i=0;i<arr.length;i++) {
            if(arr[i].status=="Open") {
                openTickets[j]=arr[i].name
                j=j+1
            }
        }
        Passenger.find({"BusID":busID})
        .then(response => {
            let closeTickets=response;
        
            res.json({
                "open_Tickets":openTickets,
                "closed_Tickets":closeTickets
            })
        })
    })
    .catch(error => {
        res.json({
            message:'Bus finding error'
        })   
    })
}
const clear = (req,res,next) => {
    let busID=req.params.BusID;
    
    Bus.findById(busID)
    .then(response => {
        let BusArray=response.IsBooked;
        for(var i=0;i<BusArray.length;i++) {
            if(BusArray[i].status=="closed") {
                BusArray[i].status="Open"
            }
        }
        let updateddata= {
            "IsBooked":BusArray
        }
        Bus.findByIdAndUpdate(busID,{$set : updateddata},{new:true})
        .then(() => {
            res.json({
                message:'IsBooked updated Successfully'
            })
        })
        .catch(error =>{
            res.json({
                message:"Error updating the IsBooked!"
            })
        })
        let BS=response.BusSeats
        let ctn=40-BS
        let updatedData = {
            BusSeats: BS+ctn
        }
        Bus.findByIdAndUpdate(busID,{$set : updatedData},{new:true})
        .then(() => {
            res.json({
                message:'Bus Truncated Successfully'
            })
        })
        .catch(error =>{
            res.json({
                message:"Error updating the number of seats!"
            })
        })
        Passenger.deleteMany({BusID:busID})
        .then(response => {
            res.json({
                message:"Passenger Truncated Successfully"
            })
        })
    })
    .catch(error => {
        res.json({
            message:'Bus Truncation error'
        })   
    }) 
}

module.exports = {
    index, show, store, update, destroy, clear,countTickets
}


// BusName:req.body.BusName,
// BusType:req.body.BusType,
// BusStartTiming:req.body.BusStartTiming,
// BusEndTiming:req.body.BusEndTiming,
// BusDate:req.body.BusDate,
// BusSource:req.body.BusSource,
// BusDestination:req.body.BusDestination,
// BusFeatures:req.body.BusFeatures,
// BusRating:req.body.BusRating



