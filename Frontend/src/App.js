import Home from './components/User/Home';
import {Booking} from './components/User/Booking';
import Admin from './components/Admin/Admin';
import {AddBus} from './components/Admin/AddBus';
import Open from './components/Admin/Open';
import Close from './components/Admin/Close';
import Truncate from './components/Admin/Truncate';
import {Update} from './components/Admin/Update';
import Delete from './components/Admin/Delete';
import View from './components/Admin/View';
import User from './components/User/UserLogin';
import {BrowserRouter as Router, Route} from 'react-router-dom';
import UserSignUp from './components/User/SignUp';
import Cart from './components/User/Cart';
import Profile from './components/User/Profile';;

const App=() => {
  return(
    <Router> 
      <Route path='/Home' exact component={Home}></Route>
      <Route path='/Booking/:key' component={Booking}></Route>
      <Route path='/UserLogin' component={User}></Route>
      <Route path='/Admin' component={Admin}></Route>
      <Route path='/AddBus' component={AddBus}></Route>
      <Route path='/OpenSeats/:key' component={Open}></Route>
      <Route path='/ClosedSeats/:key' component={Close}></Route>
      <Route path='/TruncateBus/:key' component={Truncate}></Route>
      <Route path='/UpdateBus/:key' component={Update}></Route>
      <Route path='/DeleteBus/:key' component={Delete}></Route>
      <Route path='/ViewBus' component={View}></Route>
      <Route path='/MyBookings' component={Cart}></Route>
      <Route path='/Profile' component={Profile}></Route>
      <Route path='/'  exact component ={User} />
      <Route path='/Login'  exact component ={User} />
      <Route path="/SignUp" component={UserSignUp}></Route>
    </Router>
  );
}

export default App;
