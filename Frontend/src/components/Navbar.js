import { useState } from 'react';
import Modal from 'react-bootstrap/Modal'
import Button from 'react-bootstrap/Button' 
const Navbar = () => {
    const handleHome =() => {
        window.location.href="/Home";
    }
    const handleProfile = () => {
        window.location.href="/Profile";
    }
    const handleCart = () => {
        window.location.href="/MyBookings";
    }
    const handleLogOut= () => {
        sessionStorage.clear();
        setModalShow(true);
    }
    const handleLogOutt= () => {
        setModalShow(false);
        window.location.href='/Login';
    }
    const [modalShow, setModalShow] = useState(false);
    return (
        <>
            <nav className="navbar">
                <h1>Bus Booking</h1>
                <div className="links">
                    <button onClick={handleHome}>Home</button>
                    <button onClick={handleProfile}>User Profile</button>
                    <button onClick={handleCart}>View Cart</button>
                    <button onClick={handleLogOut}>LogOut</button>
                </div>
            </nav>
            <Modal
                show={modalShow}
                backdrop="static"
                keyboard={false}
                fade={true}
                animation={true}
                >
                <Modal.Header>
                <Modal.Title>LogOut</Modal.Title>
                </Modal.Header>
                <Modal.Body>
                    Thanks for your time!
                </Modal.Body>
                <Modal.Footer>
                <Button variant="primary" onClick={handleLogOutt}>Ok</Button>
                </Modal.Footer>
            </Modal>
        </>
    );
}
 
export default Navbar;