import Navbar from '../Navbar';
import '../../index.css';
import React from 'react';
import axios from 'axios';
import Modal from 'react-bootstrap/Modal'
import Button from 'react-bootstrap/Button'
import ModalDialog from 'react-bootstrap/ModalDialog'
import ModalHeader from 'react-bootstrap/ModalHeader'
import ModalTitle from 'react-bootstrap/ModalTitle'
import ModalBody from 'react-bootstrap/ModalBody'
import ModalFooter from 'react-bootstrap/ModalFooter'

export class Booking extends React.Component {
    constructor(props) {
        super(props)
        
        this.state={
            PassName:'',
            PassAge:'',
            PassGender:'',
            PassSeat:0,
            BusID:'',
            Seats:[],
            ShowModal:false,
            SeatAlign:0
        }
        this.handleChangePassName=this.handleChangePassName.bind(this);
        this.handleChangePassAge=this.handleChangePassAge.bind(this);
        this.handleChangePassGender=this.handleChangePassGender.bind(this);
        this.submitPassForm=this.submitPassForm.bind(this);
        this.currentSeat=this.currentSeat.bind(this);
    }
    handleChangePassName(e) {
        this.setState({PassName:e.target.value})
    }
    handleChangePassAge(e) {
        this.setState({PassAge:e.target.value})
    }
    handleChangePassGender(e) {
        this.setState({PassGender:e.target.value})
    }
    currentSeat(seatNo) {
        this.setState({PassSeat:seatNo,ShowModal:true})
    }
    
    submitPassForm(e) {
        e.preventDefault();
        
        var data = {
            "BusID":this.props.match.params.key.toString(),
            "UserID":sessionStorage.getItem('UserID'),
            "PassName":this.state.PassName,
            "PassAge":parseInt(this.state.PassAge),
            "PassGender":this.state.PassGender,
            "PassSeat":parseInt(this.state.PassSeat)
        }
        // {headers: {'Content-Type': 'application/json',Accept: "application/json"}}
        axios.put('http://localhost:3002/api/Passenger/Passstore', data)
        .then((response) => {
            this.setState({ShowModal:false});
            if(response) {
                console.log(response);
                alert("Passenger Added Successfully");                
                this.props.history.push("/Home");
            }
            else {
                alert("Failed to Add Passenger");
                this.props.history.push("/Home");
            }
        }).catch((error) => {
            console.log(error);
        });
    }
    componentDidMount() {
        axios.get('http://localhost:3002/api/Bus')
        .then((response) => {
            console.log(response.data.response);
            this.setState({Seats:response.data.response[0].IsBooked})
        })
    }

    render() {
        return (
            <div className="App">
                <Navbar></Navbar>
                <Modal
                bsStyle="primary"
                style={{opacity:1}}
                fade={true}
                animation={true}
                show={this.state.ShowModal}
                size="lg"
                aria-labelledby="contained-modal-title-vcenter"
                centered
                >
                
                <ModalHeader>
                    <ModalTitle id="contained-modal-title-vcenter">
                    <h4>Passenger Details</h4> 
                    </ModalTitle>
                </ModalHeader>
                <ModalBody>
                    <div className="container">
                        <div className="row">
                            <div className="col">
                                <label htmlFor="PassName">Passenger Name</label>
                            </div>
                            <div className="col">
                            <input type="text" name="PassName" id="PassName" value={this.state.PassName} onChange={this.handleChangePassName} placeholder="Enter Passenger Name"/>  
                            </div>
                        </div>
                        <div className="row">
                            <div className="col">
                                <label htmlFor="Age"> Age</label>
                            </div>
                            <div className="col">
                            <input type="number" name="PassAge" id="PassAge" value={this.state.PassAge} onChange={this.handleChangePassAge} placeholder="Enter Age"/> 
                            </div>
                        </div>
                        <div className="row">
                            <div className="col">
                                <label htmlFor="Gender">Gender </label>
                            </div>
                            <div className="col">
                                <select name="PassGender" id="PassGender" value={this.state.PassGender} onChange={this.handleChangePassGender}>
                                    <option value="default">--Select Gender--</option>
                                    <option value="Male">Male</option>
                                    <option value="Female">Female</option>
                                    <option value="TG">Transgender</option>
                                </select>
                            </div>
                        </div>
                        <div className="row">
                            <div className="col">
                            <td> <Button type="submit" onClick={ (e) => this.submitPassForm(e)}  style={{color:"white",backgroundColor:"#f1356d",borderRadius:"8px",padding: "10px",marginLeft:"250px",marginTop:"30px",width:"130px",fontSize:"15px",fontWeight:"bold"}}>Confirm Seat</Button></td>
                            </div>
                            
                        </div>
                    </div>
                </ModalBody>
                <ModalFooter>
                    <Button variant="warning" style={{borderRadius:"8px",padding: "10px",fontSize:"15px",fontWeight:"bold"}} onClick={() => {this.setState({ShowModal:false})}}>Close</Button>
                </ModalFooter>
                </Modal>
                <div className="content">
                    <div className="Booking">
                        {
                            this.state.Seats.map((item,index) => {
                                return(
                                    <>
                                        <Button style={{borderRadius:"8px",padding: "10px",margin:"10px",marginRight:"50px",width:"50px",fontSize:"15px",fontWeight:"bold"}} variant="outline-primary" onClick={() => this.currentSeat(item.name)} disabled={item.status==='closed'?true:false}>{item.name}</Button>
                                    </>
                                )
                            })
                        }
                        
                    </div>
                </div>
            </div>
        );
    }
}