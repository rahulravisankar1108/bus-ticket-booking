import Button from 'react-bootstrap/Button';
import '../../index.css';
import "bootstrap/dist/css/bootstrap.min.css";
import Navbar from '../Navbar';
import axios from 'axios';
import { useEffect, useState } from 'react';

const Profile = (props) => {
    const [UserID,setUserID]=useState();
    setUserID(sessionStorage.getItem('UserID'));
    const [UserDetails, setUserDetails]=useState({});
    const [Loading, setLoading] = useState(false);
    useEffect(() => {
        axios.get('http://localhost:3002/api/User/Profile/'+UserID.toString()).then((response) => {
            setUserDetails(response.data);
            console.log(response.data);
            setLoading(true);
        })
    },[])

    if(Loading) {
        return(
            <div>
                <Navbar></Navbar>
                <div className="content">
                    {UserDetails.response.map((value) => {
                        return(
                            <>
                                <div className="row">
                                    <div className="col">Email</div>
                                    <div className="col">{value.Email} </div>
                                </div>
                                <div className="row">
                                    <div className="col">Name</div>
                                    <div className="col">{value.Name} </div>
                                </div>
                                <div className="row">
                                    <div className="col">Phone</div>
                                    <div className="col">{value.Phone} </div>
                                </div>
                                <div className="row">
                                    <div className="col">Address</div>
                                    <div className="col">{value.Address}</div>
                                </div>
                                <div className="row">
                                    <div className="col">City</div>
                                    <div className="col">{value.City} </div>
                                </div>
                                <div className="row">
                                    <div className="col">Pincode</div>
                                    <div className="col">{value.Pincode} </div>
                                </div>
                                <div className="row">
                                    <div className="col"><Button>Update Details</Button> </div>
                                    <div className="col"><Button>Back</Button> </div>
                                </div>
                            </>
                        );
                    })}    
                </div>
            </div>
            
        );
    }
    else{
        return(
            <div>
                Error!
            </div>
        );
    }
}

export default Profile;