import '../../index.css';
import React, { useEffect, useState } from "react";
import axios from 'axios';
import Modal from 'react-bootstrap/Modal'
import Button from 'react-bootstrap/Button';
const UserLogin = (props) => { 
    const [State,setState]  = useState({
        Name:'',
        Password:''
    })
    const handleChangeName = (event) => {
        setState({Name:event.target.value,Password:State.Password})
    }
    const handleChangePassword = (event) => {
        setState({Name:State.Name,Password:event.target.value})
    }
    const LoginHandle = (event) => {
        event.preventDefault();
        const Data ={"Name":State.Name,"Password":State.Password};
        axios.post('http://localhost:3002/api/User/Login',Data).then((response) => {
            alert('You are logged In!');
            sessionStorage.setItem('UserID',response.data.response._id);
            window.location.href='/Home';
        });
    }
    const SignUpHandle = () => {
        window.location.href="/SignUp";
    }
    return (
        <div>
            <nav className="navbar">
                <h1>Login</h1>
            </nav>
            <div className="content">
                <table>
                    <tr>
                        <td>User Name</td>
                        <td> <input type="text" name="Name" value={State.Name} onChange={(e) => handleChangeName(e)} /></td>
                    </tr>
                    <tr>
                        <td>Password</td>
                        <td> <input type="password" name="Password" value={State.Password} onChange={(e) => handleChangePassword(e)} /></td>
                    </tr>
                    <tr>
                        <td><Button type="button" onClick={SignUpHandle}>Sign Up</Button></td>
                        <td><Button type="submit" onClick={LoginHandle}>Login</Button></td>
                    </tr>
                </table>
            </div>
        </div>   
    );
}

export default UserLogin;
