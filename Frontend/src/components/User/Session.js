let Session = (function() {
  var UserID = "";

  var getID = function() {
    return UserID;  
  };

  var setID = function(UserId) {
    UserID = UserId;     
  };

  return {
    getID: getID,
    setID: setID
  }
  
})();
  
export default Session;