import Navbar from '../Navbar';
import '../../index.css';
import axios from 'axios';
import Modal from 'react-bootstrap/Modal'
import Button from 'react-bootstrap/Button'
import { useEffect, useState } from 'react';

const Cart = ()=> {
    const UserID=sessionStorage.getItem('UserID');
    const [Passenger, setPassenger]=useState([]);
    const [BusDetails,setBusDetails]=useState([]);
    const [Loading, setLoading] = useState(false);
    const [Loadingg, setLoadingg] = useState(false);
    const [BusID,setBusID]=useState();
    useEffect(() => {
        axios.get('http://localhost:3002/api/User/MyBooking/'+UserID.toString()).then(response => {
            setPassenger(response.data.response);
            setBusID(response.data.response.PassName);
            setLoading(true);
        })
    },[])
    
    console.log(Passenger);
    // Passenger.map(value => { 
    //     if(value!=null)
    //     {   axios.get('http://localhost:3002/api/Bus/Show/'+value.BusID).then(response => {
    //             setBusDetails(response.data.response);
    //             console.log(BusDetails);
    //         })
    //     }
    // })

    if(Loading) {
        return(
            <div className="App">
                <Navbar></Navbar>
                <div className="content">
                    {Passenger.map(value => {
                        return(
                            <div className="container">
                                <div className="row">
                                    <div className="col">Passenger Details</div> 
                                    <div className="col">
                                        <div className="row">
                                            <div className="col"> Name</div>
                                            <div className="col"> {value.PassName} </div>
                                        </div>
                                        <div className="row">
                                            <div className="col"> Age</div>
                                            <div className="col"> {value.PassAge} </div>
                                        </div>
                                        <div className="row">
                                            <div className="col"> Gender</div>
                                            <div className="col"> {value.PassGender} </div>
                                        </div>
                                        <div className="row">
                                            <div className="col"> Seat Number</div>
                                            <div className="col"> {value.PassSeat} </div>
                                        </div>
                                    </div>
                                    
                                </div>
                                {BusDetails.map(value => {
                                    return(
                                        <div className="row">
                                            <div className="col">Bus Details</div>
                                            <div className="col">
                                                <div className="row">
                                                    <div className="col">Name</div>
                                                    <div className="col">{value.BusName} </div>
                                                </div>  
                                                <div className="row">
                                                    <div className="col">Type</div>
                                                    <div className="col">{value.BusType} </div>
                                                </div>
                                                <div className="row">
                                                    <div className="col">Source</div>
                                                    <div className="col"> {value.BusSource} </div>
                                                </div>
                                                <div className="row">
                                                    <div className="col">Destination</div>
                                                    <div className="col">{value.BusDestination} </div>
                                                </div>
                                                <div className="row">
                                                    <div className="col">Start Timing</div>
                                                    <div className="col">{value.BusStartTimings} </div>
                                                </div>
                                                <div className="row">
                                                    <div className="col">End Timing</div>
                                                    <div className="col">{value.BusEndTimings} </div>
                                                </div>
                                                <div className="row">
                                                    <div className="col">Date of Journey</div>
                                                    <div className="col">{value.BusDate} </div>
                                                </div>
                                                <div className="row">
                                                    <div className="col">Fare</div>
                                                    <div className="col">{value.BusFare} </div>
                                                </div>
                                            </div>
                                        </div>
                                    );
                                })}
                                
                            </div>
                        );
                    })}
                    
                </div>
            </div>
        );
    }
    else{
        return(
            <div>
                Error!
            </div>
        );
    }
}

export default Cart;