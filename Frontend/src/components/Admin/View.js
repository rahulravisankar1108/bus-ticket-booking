import './style.css';
import axios from 'axios';
import { useEffect, useState } from 'react';

const View = (props) => {

    const [Loading, setLoading] = useState(false);
    const handleDelete= (BusID) => {
        
        axios.get('http://localhost:3002/api/Bus/delete/'+BusID)
        .then((response) => {
            if(response) {
                alert("Bus Deleted Successfully!");
                props.history.push('/ViewBus');
            }
            else {
                alert("Failed to Delete Bus");
                props.history.push('/ViewBus');
            }
        })
        setLoading(false);
            
    }
    const handleUpdate=(BusID) => {
        
        window.location.href="/UpdateBus/"+BusID;
        setLoading(false);
    }
    const handleViewOpenSeats=(BusID) => {
        
        window.location.href="/OpenSeats/"+BusID;
        setLoading(false);
    }
    const handleViewClosedSeats=(BusID) => {

        window.location.href="/ClosedSeats/"+BusID;
        setLoading(false);
    }
    const handleTruncateBus=(BusID) => {

        axios.get('http://localhost:3002/api/Bus/clear/'+BusID)
        .then((response) => {
            if(response) {
                alert("Bus Truncated Successfully!");
                window.location.href="/ViewBus";
            }
            else {
                alert("Failed to Delete Bus");
                window.location.href="/ViewBus";
            }
        })
        setLoading(false);
    }
    const LogOutFun = () => {
        
        alert('Admin Logged out Successfully!');
        props.history.push("/Admin");
        setLoading(false);
    }
    const AddBus = () => {
        window.location.href="/AddBus";
    }
    
    const [Bus, setBus]=useState([]);
    useEffect(() => {
        axios.get('http://localhost:3002/api/Bus').then((response) => {
            console.log(response);
            setBus(response.data);
            setLoading(true);
        })
    },[])
    if(Loading) {
        return (
            <>
                <nav className="navbar">
                    <h1>Welcome Admin</h1>
                    <div>
                        <button onClick={AddBus}>Add new Bus</button>
                        <button onClick={LogOutFun}>LogOut</button>
                    </div>
                </nav>
                <div className="App">
                    <div className="content">
                        {Bus.response.map((value,key) => {
                            
                            return(
                                <div className="home">
                                    <table>
                                        <tr style={{
                                            padding: "20px"
                                        }}>
                                            <td>Bus Name</td>
                                            <td>{value.BusName}</td>
                                        </tr>
                                        <tr>
                                            <td>Type</td>
                                            <td>{value.BusType}</td>
                                        </tr>
                                        <tr>
                                            <td>Source Timing</td>
                                            <td>{value.BusStartTiming}</td>
                                        </tr>
                                        <tr>
                                            <td>Destination Timing</td>
                                            <td>{value.BusEndTiming} </td>
                                        </tr>
                                        <tr>
                                            <td>Date Of Running</td>
                                            <td>{value.BusDate}</td>
                                        </tr>
                                        <tr>
                                            <td>Source</td>
                                            <td>{value.BusSource}</td>
                                        </tr>
                                        <tr>
                                            <td>Destination</td>
                                            <td>{value.BusDestination} </td>
                                        </tr>
                                        <tr>
                                            <td>Features</td>
                                            <td>{value.BusFeatures}</td>
                                        </tr>
                                        <tr>
                                            <td>Available Seats</td>
                                            <td>{value.BusSeats}</td>
                                        </tr>
                                        <tr>
                                            <td>Amount </td>
                                            <td>₹ {value.BusFare}</td>
                                        </tr>
                                        <tr>
                                            <td><button style={{color:"white",backgroundColor:"#f1356d",borderRadius:"8px",padding: "10px",fontSize:"15px",fontWeight:"bold",alignItems:"left"}} onClick ={()=> handleUpdate(value._id)}>Update Bus</button>
                                            <button style={{color:"white",backgroundColor:"#f1356d",borderRadius:"8px",padding: "10px",fontSize:"15px",fontWeight:"bold",alignItems:"left"}} onClick ={()=> handleDelete(value._id)}>Delete Bus</button></td>
                                            <td><button style={{color:"white",backgroundColor:"#f1356d",borderRadius:"8px",padding: "10px",fontSize:"15px",fontWeight:"bold",alignItems:"left"}}  onClick ={()=> handleViewOpenSeats(value._id)}>View Open Seats</button>
                                            <button style={{color:"white",backgroundColor:"#f1356d",borderRadius:"8px",padding: "10px",fontSize:"15px",fontWeight:"bold",alignItems:"left"}} onClick ={()=> handleViewClosedSeats(value._id)}>View Closed Seats</button> </td>
                                            
                                        </tr>
                                        <tr>
                                        <td><button style={{color:"white",backgroundColor:"#f1356d",borderRadius:"8px",padding: "10px",fontSize:"15px",fontWeight:"bold",alignItems:"left"}} onClick ={()=> handleTruncateBus(value._id)}>Reset Bus</button></td>
                                        </tr>
                                    </table>
                                </div>
                            );
                        })} 
                    </div>
                </div>
            </>
        );
        
    }
    else {
        return(
            <div>
            </div>
        );
    }
    
}
  
export default View;