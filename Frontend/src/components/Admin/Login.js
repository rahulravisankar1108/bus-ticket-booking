import './style.css';
import React from "react";

export default class Login extends React.Component {
    LoginHandle = (e) => {
      e.preventDefault();
      
      if(this.refs.UserName.value==='ADMIN' && this.refs.Password.value==='ADMIN@123') {
          window.location.href='/ViewBus';
      }
      else {
          window.location.href='/Admin';
      }
    }
    render() {
        return (
            <div>
                <nav className="navbar">
                    <h1>Admin Login</h1>
                </nav>
                <div className="content">
                    <table>
                        <tr>
                            <td>User Name</td>
                            <td> <input type="text" name="UserName" ref="UserName"/></td>
                        </tr>
                        <tr>
                            <td>Password</td>
                            <td> <input type="password" name="Password" ref="Password"/></td>
                        </tr>
                        <tr aria-colspan="2">
                            <td><button type="submit" onClick={this.LoginHandle}>Login</button></td>
                        </tr>
                    </table>
                </div>
            </div>
            
            
        );
    }
}
