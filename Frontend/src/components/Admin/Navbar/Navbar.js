import './NavStyle.css';
const NavbarAdmin = (props) => {
    const LogOutFun = () => {
        alert('Admin Logged out Successfully!');
        window.location.href='/Admin';
    }
    const ViewBus = () => {
        window.location.href="/ViewBus";
    }
    const AddBus = () => {
        window.location.href="/AddBus";
    }
    return (
        <nav className="navbar">
            <h1>Welcome Admin</h1>
            <div>
                <button onClick={AddBus}>Add new Bus</button>
                <button onClick={ViewBus}>View Buses</button>
            {/*
                <a href="/TruncateBus">Reset Bus</a>
                <a href="/DeleteBus">Delete Bus</a>
                <a href="/OpenSeats">View Open Seats</a>
                <a href="/ClosedSeats">View Closed Seats</a> */}
                <button onClick={LogOutFun}>LogOut</button>
            </div>
        </nav>
    );
}
 
export default NavbarAdmin;