import axios from 'axios';
import NavbarAdmin from './Navbar/Navbar';
import "./style.css";
import "bootstrap/dist/css/bootstrap.min.css";
import React, { useEffect, useState } from 'react';
import Modal from 'react-bootstrap/Modal'
import Button from 'react-bootstrap/Button'
import ModalDialog from 'react-bootstrap/ModalDialog'
import ModalHeader from 'react-bootstrap/ModalHeader'
import ModalTitle from 'react-bootstrap/ModalTitle'
import ModalBody from 'react-bootstrap/ModalBody'
import ModalFooter from 'react-bootstrap/ModalFooter'
const Close = (props) => {
    const HandlePopUp = (PassName,PassAge,PassGender) => {
        let data={PassName:PassName,PassAge:PassAge,PassGender:PassGender};
        setPassenger(data);
        setModalShow(true);
    }
    const [modalShow, setModalShow] = useState(false);
    const [Bus, setBus]=useState([]);
    const [Passenger, setPassenger]=useState({PassName:'',PassAge:'',PassGender:''});
    const [Loading, setLoading] = useState(false);
    useEffect(() => {
        axios.get('http://localhost:3002/api/Bus/countTickets/'+props.match.params.key.toString()).then((response) => {   
            setBus(response.data.closed_Tickets);
            setLoading(true);
        })
    },[])
    if(Loading) {
        return (
            <div>
                <NavbarAdmin></NavbarAdmin>
                <Modal
                bsStyle="primary"
                style={{opacity:1}}
                fade={true}
                animation={true}
                show={modalShow}
                size="lg"
                aria-labelledby="contained-modal-title-vcenter"
                centered
                >
                
                <ModalHeader>
                    <ModalTitle id="contained-modal-title-vcenter">
                    <h4>Passenger Details</h4> 
                    </ModalTitle>
                </ModalHeader>
                <ModalBody>
                    <table>
                        <tbody>
                            <tr><th>Passenger Name</th>
                                <td>{Passenger.PassName}</td> 
                            </tr>
                            <tr>
                                <th>Passenger Age</th>
                                <td>{Passenger.PassAge}</td>
                            </tr>
                            <tr>
                                <th>Passenger Gender</th>
                                <td>{Passenger.PassGender}</td>
                            </tr>
                        </tbody>
                    </table>
                </ModalBody>
                <ModalFooter>
                    <Button style={{color:"white",backgroundColor:"#f1356d",borderRadius:"8px",padding: "10px",fontSize:"15px",fontWeight:"bold",alignItems:"left"}} onClick={() => {setModalShow(false)}}>Close</Button>
                </ModalFooter>
                </Modal>
                <div className="content">
                    <h1>Closed Seats</h1>
                    {Bus.map((value) => {
                        return (
                        <> 
                            <Button variant="primary" onClick={() => HandlePopUp(value.PassName,value.PassAge,value.PassGender)}>{value.PassSeat}</Button>
                        </>
                        );
                    })}   
                </div>
            </div>
        );
    }
    else {
        return(
            <div>
            
        </div>
        );
        
    }
}
 
export default Close;