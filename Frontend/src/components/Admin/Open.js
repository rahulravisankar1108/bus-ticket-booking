import axios from 'axios';
import NavbarAdmin from './Navbar/Navbar';
import "./style.css";
import React, { useEffect, useState } from 'react';
const Open = (props) => {
    const [Bus, setBus]=useState([]);
    const [Loading, setLoading] = useState(false);
    useEffect(() => {
        axios.get('http://localhost:3002/api/Bus/countTickets/'+props.match.params.key.toString()).then((response) => {
            setBus(response.data.open_Tickets);
            setLoading(true);
        })
    },[])
    if(Loading) {
        return (
            <div>
                <NavbarAdmin></NavbarAdmin>
                <div className="content">
                    <h1>Open Seats</h1>
                    {Bus.map((value,key) => {
                        return(
                            <>
                                 <button>{value}</button>
                            </>
                        );
                    })}   
                </div>
            </div>
        );
    }
    else {
        return(
            <div>
                Error
            </div>
        );
        
    }
}
 
export default Open;