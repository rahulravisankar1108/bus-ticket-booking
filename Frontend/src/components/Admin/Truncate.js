import NavbarAdmin from './Navbar/Navbar';
import './style.css';
import axios from 'axios';
import { useEffect, useState } from 'react';

const Truncate = () => {

    const handleClick= (BusID) => {
        window.location.href='/Booking/'+BusID;
    }
    const [Bus, setBus]=useState([]);
    
    const [Loading, setLoading] = useState(false);
    useEffect(() => {
        axios.get('http://localhost:3002/api/Bus').then((response) => {
            console.log(typeof(response.data));
            setBus(response.data);
            setLoading(true);
        })
    },[])
    if(Loading) {
        return (
            <div className="App">
                <NavbarAdmin></NavbarAdmin>
                <div className="content">
                    {Bus.response.map((value,key) => {
                        console.log(value,key)
                        return(
                            <div className="home">
                                <table>
                                    <tr style={{
                                        padding: "20px"
                                    }}>
                                        <td>Bus Name</td>
                                        <td>{value.BusName}</td>
                                    </tr>
                                    <tr>
                                        <td>Type</td>
                                        <td>{value.BusType}</td>
                                    </tr>
                                    <tr>
                                        <td>Source Timing</td>
                                        <td>{value.BusStartTiming}</td>
                                    </tr>
                                    <tr>
                                        <td>Destination Timing</td>
                                        <td>{value.BusEndTiming} </td>
                                    </tr>
                                    <tr>
                                        <td>Date Of Running</td>
                                        <td>{value.BusDate}</td>
                                    </tr>
                                    <tr>
                                        <td>Source</td>
                                        <td>{value.BusSource}</td>
                                    </tr>
                                    <tr>
                                        <td>Destination</td>
                                        <td>{value.BusDestination} </td>
                                    </tr>
                                    <tr>
                                        <td>Features</td>
                                        <td>{value.BusFeatures}</td>
                                    </tr>
                                    <tr>
                                        <td>Available Seats</td>
                                        <td>{value.BusSeats}</td>
                                    </tr>
                                    <tr>
                                        <td>Amount </td>
                                        <td>₹ {value.BusFare}</td>
                                    </tr>
                                    
                                        <tr aria-colspan="2">
                                            <button onClick ={()=> handleClick(value._id)}>Book Seats</button>
                                        </tr>
                                </table>
                            </div>
                        );
                    })} 
                </div>
            </div>
        );
    }
    else {
        return(
            <div>
            </div>
        );
    }
}
  
export default Truncate;